#!/usr/bin/env python3
import sys
import calculate

print(sys.argv)

result = calculate.calc(
    int(sys.argv[1]),
    int(sys.argv[2]),
    sys.argv[3])
if result is None:
    print('Invalid calculation!')
else:
    print('the answer is', result)
